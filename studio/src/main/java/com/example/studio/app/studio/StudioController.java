package com.example.studio.app.studio;

import java.util.Objects;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudioController {

	 @RequestMapping(value = "/")
	 public ModelAndView loginPage() {
		 ModelAndView mv = new ModelAndView();
		 mv.setViewName("/studio/login.html");
	     return mv;
	 }

	 @RequestMapping(value = "/loggedin")
	 public ModelAndView loggedin(@RequestParam(value="userId", required = true) String userId
			 ,@RequestParam(value="password", required = true) String password
			 ) {
		 ModelAndView mv = new ModelAndView();

		// 日時を取得、設定
		mv.addObject("userId", userId);

		 mv.setViewName("/studio/selectStudio.html");
	     return mv;
	 }

	 @RequestMapping(value = "/registPage")
	 public ModelAndView registPage() {
		 ModelAndView mv = new ModelAndView();
		 mv.setViewName("/studio/regist.html");
	     return mv;
	 }

	 @RequestMapping(value = "/registAndLogin")
	 public ModelAndView registAndLogin(@RequestParam(value="userId", required = true) String userId
			 ,@RequestParam(value="password", required = true) String password
			 ,@RequestParam(value="repassword", required = true) String repassword) {
		 ModelAndView mv = new ModelAndView();

		if(!Objects.equals(password, repassword)) {
			mv.addObject("errors", "パスワードが異なります。");
			mv.setViewName("/studio/regist.html");
		}

		// 日時を取得、設定
		mv.addObject("userId", userId);
		 mv.setViewName("/studio/selectStudio.html");
	     return mv;
	 }

}
